from models import Gallery, GalleryImage
from admin_view import gallery_admin_change_view
from admin_view import gallery_admin_search_images
from admin_view import gallery_admin_upload_batch
from admin_view import gallery_admin_upload_one
from admin_view import gallery_admin_confirm_upload
from admin_view import gallery_admin_delete_image
from admin_view import gallery_admin_edit_image
from django.conf.urls import patterns
from sorl.thumbnail.admin import AdminImageMixin
from django.contrib import admin


class GalleryAdmin(AdminImageMixin, admin.ModelAdmin):
    #list_display = ('title', 'image_thumb')
    def change_view(self, request, gallery_id):
        return gallery_admin_change_view(request, self, gallery_id)

    def search_images(self, request):
        return gallery_admin_search_images(request, self)

    def upload_batch(self, request, gallery_id):
        return gallery_admin_upload_batch(request, self, gallery_id)

    def upload_one(self, request, gallery_id):
        return gallery_admin_upload_one(request, self, gallery_id)

    def confirm_upload(self, request, gallery_id):
        return gallery_admin_confirm_upload(request, self, gallery_id)

    def delete_image(self, request):
        return gallery_admin_delete_image(request, self)

    def edit_image(self, request):
        return gallery_admin_edit_image(request, self)

    def get_urls(self):
        urls = super(GalleryAdmin, self).get_urls()
        my_urls = patterns('',
                           ('^(?P<gallery_id>\d+)/$', self.change_view),
                           ('^upload_batch/(?P<gallery_id>\d+)/$',
                            self.upload_batch),
                           ('^upload_one/(?P<gallery_id>\d+)/$',
                            self.upload_one),
                           ('^confirm_batch/(?P<gallery_id>\d+)/$',
                            self.confirm_upload),
                           ('^delete_image/$', self.delete_image),
                           ('^edit_image/$', self.edit_image),
                           ('^search/$', self.search_images),
                           )

        return my_urls + urls


class GalleryImageAdmin(AdminImageMixin, admin.ModelAdmin):
    pass

admin.site.register(Gallery, GalleryAdmin)
#admin.site.register(GalleryImage, GalleryImageAdmin)

