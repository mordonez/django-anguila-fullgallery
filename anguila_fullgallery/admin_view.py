from django.shortcuts import render_to_response, redirect
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.forms import ModelForm
from django.contrib.admin import helpers
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from StringIO import StringIO
from django.core.serializers.json import Serializer
from django.core.paginator import Paginator, EmptyPage
import json
import zipfile
from django.core.files.base import ContentFile
from anguila_fullgallery.models import GalleryImage, Gallery
from django.conf import settings

# Required PIL classes may or may not be available from the root namespace
# depending on the installation method used.
try:
    import Image
    import ImageFile
    import ImageFilter
    import ImageEnhance
except ImportError:
    try:
        from PIL import Image
        from PIL import ImageFile
        from PIL import ImageFilter
        from PIL import ImageEnhance
    except ImportError:
        raise ImportError(_('Anguila Full Gallery was unable to import the Python Imaging Library. Please confirm it`s installed and available on your current Python path.'))


class SerializerHelper(Serializer):
    def serialize(self, queryset, list_of_attributes, **options):
        self.options = options
        self.stream = options.get("stream", StringIO())
        self.start_serialization()
        for obj in queryset:
            self.start_object(obj)
            for field in list_of_attributes:
                self.handle_field(obj, field)
            self.end_object(obj)
        self.end_serialization()
        return self.getvalue()

    def handle_field(self, obj, field):
        self._current[field] = getattr(obj, field)


class GalleryForm(ModelForm):
    class Meta:
        model = Gallery


def gallery_admin_change_view(request, model_admin, gallery_id):
    opts = model_admin.model._meta
    admin_site = model_admin.admin_site
    has_change_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_change_permission()
    )
    has_delete_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_delete_permission()
    )
    has_add_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_add_permission()
    )

    gallery = Gallery.objects.get(pk=gallery_id)
    form = GalleryForm(instance=gallery)

    adminForm = helpers.AdminForm(
        form,
        list(model_admin.get_fieldsets(request)),
        model_admin.prepopulated_fields
    )

    media = model_admin.media + adminForm.media
    change_url = reverse(
        "admin:" + str(opts).replace(".", "_") + "_changelist"
    )

    upload_batch_url = "/"
    upload_batch_url = upload_batch_url + str(opts).replace(".", "/")
    upload_batch_url = upload_batch_url + "/upload_batch/"
    upload_batch_url = upload_batch_url + str(gallery_id)

    upload_one_url = "/"
    upload_one_url = upload_one_url + str(opts).replace(".", "/")
    upload_one_url = upload_one_url + "/upload_one/"
    upload_one_url = upload_one_url + str(gallery_id)

    context = {
        'admin_site': admin_site.name,
        'title': _('Add/Edit Galeria'),
        'opts': opts,
        'change': change_url,
        'is_change_template': True,
        'add': reverse("admin:" + str(opts).replace(".", "_") + "_add"),
        'is_popup': ('_popup' in request.REQUEST),
        'save_as': model_admin.save_as,
        'app_label': opts.app_label,
        'upload_batch_url': upload_batch_url,
        'upload_one_url': upload_one_url,
        'adminform': adminForm,
        'media': mark_safe(media),
        'prepopulated_fields': {},
        'has_delete_permission': has_change_perm,
        'has_add_permission': has_add_perm,
        'has_change_permission': has_delete_perm
    }
    template = 'admin/anguila_fullgallery/gallery/change_form.html'
    return render_to_response(template, context,
                              context_instance=RequestContext(request))


@login_required
@csrf_exempt
@staff_member_required
def gallery_admin_search_images(request, model_admin):
    if request.is_ajax():
        if request.method == "POST":
            gallery = Gallery.objects.get(pk=request.POST.get("gid"))
            images = GalleryImage.objects.filter(gallery=gallery)
            images = images.order_by("-id")
            images = images.filter(title__contains=request.POST.get("w"))

            page = 1
            if 'page' in request.POST:
                page = int(request.POST.get("page"))

            try:
                settings.ANGUILA_FULLGALLERY_ITEMS_PER_PAGE
            except Exception:
                raise Exception(
                    _('ANGUILA_FULLGALLERY_ITEMS_PER_PAGE not found.')
                )
            p = Paginator(
                images,
                settings.ANGUILA_FULLGALLERY_ITEMS_PER_PAGE
            )
            try:
                current_page = p.page(page)
            except EmptyPage:
                if page > 1:
                    page = page - 1
                    current_page = p.page(page)

            data = SerializerHelper().serialize(
                current_page.object_list, ["title", 'image_thumb']
            )

            data = {'numpages': p.num_pages,
                    'current_page': page,
                    'data': json.loads(data)
                    }

            return HttpResponse(json.dumps(data))


@login_required
@csrf_exempt
@staff_member_required
def gallery_admin_upload_batch(request, model_admin, gallery_id):
    opts = model_admin.model._meta
    admin_site = model_admin.admin_site

    has_change_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_change_permission()
    )
    has_delete_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_delete_permission()
    )
    has_add_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_add_permission()
    )

    change_url = reverse(
        "admin:" + str(opts).replace(".", "_") + "_changelist"
    )

    context = {
        'admin_site': admin_site.name,
        'title': _('Add/Edit Galeria'),
        'opts': opts,
        'change': change_url,
        'add': reverse("admin:" + str(opts).replace(".", "_") + "_add"),
        'is_popup': ('_popup' in request.REQUEST),
        'save_as': model_admin.save_as,
        'app_label': opts.app_label,
        'prepopulated_fields': {},
        'has_delete_permission': has_change_perm,
        'has_add_permission': has_add_perm,
        'has_change_permission': has_delete_perm
    }

    if request.method == "GET":
        t = 'admin/anguila_fullgallery/gallery/upload_batch.html'
        return render_to_response(t, context,
                      context_instance=RequestContext(request))

    else:
        zfile = request.FILES["zip_file"]
        with open('/tmp/' + zfile.name, 'wb+') as destination:
            for chunk in zfile.chunks():
                destination.write(chunk)
        zip = zipfile.ZipFile('/tmp/' + request.FILES["zip_file"].name)
        bad_file = zip.testzip()
        if bad_file:
            raise Exception('"%s" in the .zip archive is corrupt.' % bad_file)

        g = Gallery.objects.get(pk=gallery_id)
        created_files = []
        from cStringIO import StringIO
        for filename in sorted(zip.namelist()):
            if filename.startswith('__'):  # do not process meta files
                continue
            data = zip.read(filename)
            if len(data):
                try:
                    # the following is taken from django.newforms.fields.ImageField:
                    #  load() is the only method that can spot a truncated JPEG,
                    #  but it cannot be called sanely after verify()
                    trial_image = Image.open(StringIO(data))
                    trial_image.load()
                    # verify() is the only method that can spot a corrupt PNG,
                    #  but it must be called immediately after the constructor
                    trial_image = Image.open(StringIO(data))
                    trial_image.verify()
                except Exception, e:
                    # if a "bad" file is found we just skip it.
                    print "Bad File", e
                    continue

                gi = GalleryImage(title="", description="")
                gi.image.save(filename, ContentFile(data))
                gi.save()
                gi.gallery.add(g)
                created_files.append(gi)
        zip.close()

        context["created_files"] = created_files
        context["confirm_url"] = "/" + str(opts).replace(".", "/")
        context["confirm_url"] = context["confirm_url"] + "/confirm_batch/"
        context["confirm_url"] = context["confirm_url"] + str(gallery_id) + "/"

        t = 'admin/anguila_fullgallery/gallery/result_import_batch.html'
        return render_to_response(t, context,
                              context_instance=RequestContext(request))


@login_required
@csrf_exempt
@staff_member_required
def gallery_admin_upload_one(request, model_admin, gallery_id):
    opts = model_admin.model._meta
    admin_site = model_admin.admin_site

    has_change_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_change_permission()
    )
    has_delete_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_delete_permission()
    )
    has_add_perm = request.user.has_perm(
        opts.app_label + '.' + opts.get_add_permission()
    )

    change_url = reverse(
        "admin:" + str(opts).replace(".", "_") + "_changelist"
    )

    context = {
        'admin_site': admin_site.name,
        'title': _('Add/Edit Galeria'),
        'opts': opts,
        'change': change_url,
        'add': reverse("admin:" + str(opts).replace(".", "_") + "_add"),
        'is_popup': ('_popup' in request.REQUEST),
        'save_as': model_admin.save_as,
        'app_label': opts.app_label,
        'prepopulated_fields': {},
        'has_delete_permission': has_change_perm,
        'has_add_permission': has_add_perm,
        'has_change_permission': has_delete_perm
    }

    if request.method == "GET":
        t = 'admin/anguila_fullgallery/gallery/upload_one.html'
        return render_to_response(t, context,
                      context_instance=RequestContext(request))

    else:
        uploaded_file = request.FILES["file"]
        with open('/tmp/' + uploaded_file.name, 'wb+') as destination:
            for chunk in uploaded_file.chunks():
                destination.write(chunk)

        uploaded_file = open('/tmp/' + uploaded_file.name, 'r+')

        g = Gallery.objects.get(pk=gallery_id)
        gi = GalleryImage(title="", description="")
        gi.image.save(uploaded_file.name, ContentFile(uploaded_file.read()))
        gi.save()
        gi.gallery.add(g)

        context["created_files"] = [gi]
        context["confirm_url"] = "/" + str(opts).replace(".", "/")
        context["confirm_url"] = context["confirm_url"] + "/confirm_batch/"
        context["confirm_url"] = context["confirm_url"] + str(gallery_id) + "/"

        t = 'admin/anguila_fullgallery/gallery/result_import_batch.html'
        return render_to_response(t, context,
                              context_instance=RequestContext(request))


@login_required
@staff_member_required
def gallery_admin_confirm_upload(request, model_admin, gallery_id):

    opts = model_admin.model._meta
    if request.method == "POST":
        for image in request.POST.lists():
            if image[0] != "csrfmiddlewaretoken" and image[0] != "submit":
                gi = GalleryImage.objects.get(pk=int(image[0]))
                gi.title = image[1][0]
                gi.description = image[1][1]
                gi.save()
    redirect_url = "/" + str(opts).replace(".", "/")
    redirect_url = redirect_url + "/" + str(gallery_id) + "/"
    return redirect(redirect_url)


@login_required
@staff_member_required
def gallery_admin_delete_image(request, model_admin):
    if request.is_ajax():
        if request.method == "POST":
            gi = GalleryImage.objects.get(pk=request.POST.get("id"))
            g = Gallery.objects.get(pk=request.POST.get("gid"))
            g.galleryimage_set.remove(gi)
            gi.delete()
            return HttpResponse("01")


@login_required
@staff_member_required
def gallery_admin_edit_image(request, model_admin):
    if request.is_ajax():
        if request.method == "POST":
            if 'get_data' in request.POST:
                image = GalleryImage.objects.get(pk=request.POST.get("id"))
                data = SerializerHelper().serialize(
                    [image, ], ["title", 'description']
                )
                return HttpResponse(data)
            else:
                image = GalleryImage.objects.get(pk=request.POST.get("id"))
                image.title = request.POST.get("title")
                image.description = request.POST.get("description")
                image.save()
                return HttpResponse("01")

