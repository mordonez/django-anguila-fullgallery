===============
Anguila Gallery
===============

Gallery admin with Facebook style upload images for the Django web framework.

------------
Installation
------------

Offical releases are available from: https://bitbucket.org/mordonez_me/django-anguila-fullgallery/

Using pip:

.. parsed-literal::

    pip install -e git+https://bitbucket.org/mordonez_me/django-anguila-fullgallery.git#egg=django-anguila-fullgallery

-------------------
Source Distribution
-------------------

Clone the repository. From within the cloned directory run the following command:

.. parsed-literal::

   python setup.py install


--------------------------------
Tracking the Development Version
--------------------------------

The current development version of Anguila Gallery can be checked out via git from the project site using the following command:

    git clone git@bitbucket.org:mordonez_me/django-anguila-fullgallery.git

Then either copy the django-anguila-fullgallery directory or create a symlink to the django-anguila-fullgallery directory somewhere on your python path, such as your Django project or site-packages directory.

You can verify Anguila Gallery is available to your project by running the following commands from within your project directory:

.. parsed-literal::

    manage.py shell

    >>> import anguila_fullgallery
    >>> anguila_fullgallery.VERSION
    (1, 0, 'rc1')

------------------------------
Configure Your Django Settings
------------------------------

Add 'anguila_fullgallery' to your INSTALLED_APPS setting:

.. parsed-literal::

    INSTALLED_APPS = (
        # ...other installed applications,
        'anguila_fullgallery',

    )

**Confirm that your MEDIA_ROOT and MEDIA_URL settings are correct.**

--------------------------------
Define items per page on Gallery
--------------------------------

Set the number of items to be shown in the Gallery:

.. parsed-literal::

    ANGUILA_FULLGALLERY_ITEMS_PER_PAGE = 10 #Any number needed

----------------------------------------------
Register Anguila Gallery with the Django Admin
----------------------------------------------

Add the following to your projects urls.py file:

.. parsed-literal::

    from django.contrib import admin

    admin.autodiscover()

------------------
Sync Your Database
------------------

Run the Django 'syncdb' command to create the appropriate tables.

.. parsed-literal::

    python manage.py syncdb

---------------------
Using Anguila Gallery
---------------------

To use Anguila Gallery you must have to 2 models, a Gallery Model and GalleryImage Model (names can be anything you want), this models
extends from Gallery and Gallery Image.

.. parsed-literal::

    from anguila_fullgallery.models import Gallery


    class Gallery(Gallery):
        pass

The example above is the simpliest configuration to make use of Anguila Gallery

------------
Dependences
------------

Anguila Gallery Full requires **sorl-thumbnail** and **PIL**

------------------------------------
Additional Documentation and Support
------------------------------------

Offical docs:

    https://bitbucket.org/mordonez_me/django-anguila-fullgallery/wiki/Home

If you have any issues please register them, the official issue page is:


    https://bitbucket.org/mordonez_me/django-anguila-fullgallery/issues

**Please read wiki for information about TODO and FIXME.**

-----------
Screenshots
-----------

Anguila Full Gallery List Edit Mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: http://downloads.anguilalabs.com/full_gallery/list.png


Anguila Full Gallery Batch Upload
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: http://downloads.anguilalabs.com/full_gallery/batch_upload.png


Anguila Full Gallery After Batch Upload
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: http://downloads.anguilalabs.com/full_gallery/after_batch.png
